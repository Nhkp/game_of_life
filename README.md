<div id="top"></div>

[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->

## About The Project

<!-- ![BattlefieldMario demo movie][demo_movie] -->

A parallel implementation of Conway's Game of Life. 

This implementation is based on EasyPAP student edition which is a graphical environment designed to facilitate learning of parallel programming.

EasyPAP : <a href="https://gforgeron.gitlab.io/easypap/">website</a> - 
<a href="https://reader.elsevier.com/reader/sd/pii/S0743731521001647?token=5BD4A3E736B606C7736F0B9F3CCDA9D10BB3596FD57E5B40E3A57D18BAEBEAED790E219985C6BA9829DD98A6E2DB540A&originRegion=eu-west-1&originCreation=20220613101608">paper</a> - 
<a href="https://gitlab.com/gforgeron/easypap-se">code</a>

<!-- <p align="right">(<a href="#top">back to top</a>)</p> -->

### Built With

* [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

Refer to EasyPAP <a href="https://gforgeron.gitlab.io/easypap/doc/Getting_Started.pdf">documentation</a> to see prerequisite packages.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/Nhkp/game_of_life.git
   ```
2. Set pkg-config path
   ```sh
   pkg-config --cflags sdl2 fxt hwloc
   ```
3. Compile
   ```sh
   make
   ```

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- USAGE EXAMPLES -->
## Usage

### Launch

```sh
./run -k life -v omp_vec
```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Clément Palézis - palezis.c@gmail.com

Project Link: [https://gitlab.com/Nhkp/game_of_life](https://gitlab.com/Nhkp/game_of_life)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/cpalezis
[demo_movie]: demo.mp4
141
