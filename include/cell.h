#include "easypap.h"

static unsigned color = 0xFFFF00FF; // Living cells have the yellow color

typedef char cell_t;

static cell_t *restrict _table = NULL, *restrict _alternate_table = NULL;

static cell_t *restrict _t_change = NULL, *restrict _a_t_change = NULL;

static inline cell_t *table_cell (cell_t *restrict i, int y, int x)
{
  return i + y * DIM + x;
}
