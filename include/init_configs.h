#include <unistd.h>

#include "easypap.h"
#include "rle_lexer.h"
#include "life_utils.h"

void life_draw_guns (void);

static inline void set_cell (int y, int x);

static inline int get_cell (int y, int x);

static void inline life_rle_parse (char *filename, int x, int y,
                                   int orientation);

static void inline life_rle_generate (char *filename, int x, int y, int width,
                                      int height);


void life_draw (char *param);


static void otca_autoswitch (char *name, int x, int y);

static void otca_life (char *name, int x, int y);


static void at_the_four_corners (char *filename, int distance);

// Suggested cmdline: ./run -k life -s 2176 -a otca_off -ts 64 -r 10 -si
void life_draw_otca_off (void);

// Suggested cmdline: ./run -k life -s 2176 -a otca_on -ts 64 -r 10 -si
void life_draw_otca_on (void);

// Suggested cmdline: ./run -k life -s 6208 -a meta3x3 -ts 64 -r 50 -si
void life_draw_meta3x3 (void);

// Suggested cmdline: ./run -k life -a bugs -ts 64
void life_draw_bugs (void);

// Suggested cmdline: ./run -k life -v omp -a ship -s 512 -m -ts 16
void life_draw_ship (void);

void life_draw_stable (void);

void life_draw_guns (void);

void life_draw_random (void);

// Suggested cmdline: ./run -k life -a clown -s 256 -i 110
void life_draw_clown (void);

void life_draw_diehard (void);
