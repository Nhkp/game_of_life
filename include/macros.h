#define cur_table(y, x) (*table_cell (_table, (y), (x)))
#define next_table(y, x) (*table_cell (_alternate_table, (y), (x)))
#define cur_change_table(y, x) (*(_t_change + y * (DIM / TILE_W) + x))
#define next_change_table(y, x) (*(_a_t_change + y * (DIM / TILE_W) + x))

// Threshold for hybrid computation
#define THRESHOLD 10