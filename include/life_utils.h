#include <sys/mman.h>

#include "macros.h"
#include "cell.h"

void life_init (void);

void life_ocl_init(unsigned* cpu_y_part, unsigned* gpu_y_part);

void life_finalize (void);

void life_refresh_img (void);
