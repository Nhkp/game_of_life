#include <omp.h>
#include <stdbool.h>
#include <string.h>
#include <sys/mman.h>
#include <immintrin.h>

#include "init_configs.h"
#include "vectorization.h"


static inline void swap_tables (void)
{
  cell_t *tmp = _table;

  _table           = _alternate_table;
  _alternate_table = tmp;

  tmp = _t_change;
  _t_change        = _a_t_change;
  _a_t_change      = tmp;
}

///////////////////////////// Sequential version (seq)

static int compute_new_state (int y, int x)
{
  unsigned n      = 0;
  unsigned me     = cur_table (y, x) != 0;
  unsigned change = 0;

  if (x > 0 && x < DIM - 1 && y > 0 && y < DIM - 1) {

    for (int i = y - 1; i <= y + 1; i++)
      for (int j = x - 1; j <= x + 1; j++)
        n += cur_table (i, j);

    n = (n == 3 + me) | (n == 3);

    if (n != me){
      change |= 1;
  }
    next_table (y, x) = n;
  }

  return change;
}

static int compute_new_state_vec (int y, int x)
{
  __m256i n = _mm256_setzero_si256 ();
  __m256i me = _mm256_loadu_si256 ((__m256i*) &cur_table(y,x));
  unsigned change = 0;

  if (y > 0 && y < DIM - 1) {
  __m256i neighbors_upper_line = _mm256_loadu_si256 ((__m256i*) &cur_table(y-1,x));
  __m256i neighbors_lower_line = _mm256_loadu_si256 ((__m256i*) &cur_table(y+1,x));

  __m256i neighbors_horizontal_sum = _mm256_add_epi8(neighbors_upper_line, me);
  neighbors_horizontal_sum = _mm256_add_epi8(neighbors_horizontal_sum, neighbors_lower_line);

  uint8_t shifting_right[33];
  shifting_right[32] = 0;
  _mm256_storeu_si256((__m256i *)shifting_right, neighbors_horizontal_sum);
  __m256i neighbors_hsum_right_shifted = _mm256_loadu_si256((__m256i *)(shifting_right+1));
  __m256i neighbors_hsum_left_shifted = _mm256_shift_left(neighbors_horizontal_sum);

  if (x > 0){
    uint8_t left_hand_pixel_neighbors[32];
    _mm256_storeu_si256((__m256i *)left_hand_pixel_neighbors, neighbors_hsum_left_shifted);
    left_hand_pixel_neighbors[0] += cur_table(y-1, x - 1)
            + cur_table(y, x - 1)
            + cur_table(y+1, x - 1);
    neighbors_hsum_left_shifted = _mm256_loadu_si256((__m256i *)left_hand_pixel_neighbors);
  }
  if (x < DIM - 32){
    uint8_t right_hand_pixel_neighbors[32];
    _mm256_storeu_si256((__m256i *)right_hand_pixel_neighbors, neighbors_hsum_right_shifted);
    right_hand_pixel_neighbors[31]+= cur_table(y-1, x + 32)
            + cur_table(y, x + 32)
            + cur_table(y+1, x + 32);
    neighbors_hsum_right_shifted = _mm256_loadu_si256((__m256i *)right_hand_pixel_neighbors);
  }

  __m256i nb_neighbors = _mm256_add_epi8(neighbors_horizontal_sum, neighbors_hsum_left_shifted);
  n = _mm256_add_epi8(nb_neighbors, neighbors_hsum_right_shifted);

  __m256i three = _mm256_set1_epi8(3);
  __m256i three_plus_me = _mm256_add_epi8(three, me);

  __m256i nb_of_neighbors = _mm256_cmpeq_epi8(n, three);
  __m256i nb_of_neighbors_plus_me = _mm256_cmpeq_epi8(n, three_plus_me);

  n = _mm256_or_si256(nb_of_neighbors, nb_of_neighbors_plus_me);
  n = _mm256_and_si256(n, _mm256_set1_epi8(1));

  __m256i mask = _mm256_cmpeq_epi8(n, me);
  mask = _mm256_cmpeq_epi8(mask, _mm256_setzero_si256());
  int test = hsum_8x32(mask);
  if (test)
    change |= 1;

  }

  uint8_t zeroing_left_border[32] = {[0 ... 31] = 1};
  uint8_t zeroing_right_border[32] = {[0 ... 31] = 1};
  zeroing_left_border[0] = 0;
  zeroing_right_border[31] = 0;
  if (x == 0)
    n = _mm256_and_si256 (_mm256_loadu_si256((__m256i *)&zeroing_left_border), n);
  if (x == DIM - 32)
    n = _mm256_and_si256 (_mm256_loadu_si256((__m256i *)&zeroing_right_border), n);


  _mm256_store_si256((__m256i *)&next_table(y, x), n);

  return change;
}

static int compute_new_state_nocheck (int y, int x)
{
  unsigned n      = 0;
  unsigned me     = cur_table (y, x) != 0;
  unsigned change = 0;

  for (int i = y - 1; i <= y + 1; i++)
    for (int j = x - 1; j <= x + 1; j++)
      n += cur_table(i, j);

  n = (n == 3 + me) | (n == 3);
  if (n != me)
    change |= 1;

  next_table(y, x) = n;

  return change;
}

int test_change(int y, int x) {
    for (int i = max(0, y - 1); i <= min(DIM / TILE_H, y + 1); i++)
        for (int j = max(0, x - 1); j <= min(DIM / TILE_W, x + 1); j++) {
            if (cur_change_table(i, j)) {
                return 1;
            }
        }
    return 0;
}

void change_set(int t){
    memset(_t_change, t, DIM/TILE_W * DIM/TILE_H * sizeof (cell_t));
}

// Tile inner computation
static int do_tile_reg (int x, int y, int width, int height)
{
  int change = 0;

  for (int i = y; i < y + height; i++)
    for (int j = x; j < x + width; j++)
      change |= compute_new_state (i, j);

  return change;
}

static int do_tile_reg_vec (int x, int y, int width, int height)
{
  int change = 0;

  for (int i = y; i < y + height; i++)
    for (int j = x; j < x + width; j+=32)
      change |= compute_new_state_vec (i, j);

  return change;
}

static int do_inner_tile_reg (int x, int y, int width, int height)
{
  int change = 0;

  for (int i = y; i < y + height; i++)
    for (int j = x; j < x + width; j++)
      change |= compute_new_state_nocheck (i, j);

  return change;
}

static int do_tile (int x, int y, int width, int height, int who)
{
  int r;

  monitoring_start_tile (who);

  r = do_tile_reg (x, y, width, height);

  monitoring_end_tile (x, y, width, height, who);

  return r;
}

static int do_tile_vec (int x, int y, int width, int height, int who)
{
  int r;

  monitoring_start_tile (who);

  r = do_tile_reg_vec (x, y, width, height);

  monitoring_end_tile (x, y, width, height, who);

  return r;
}


static int do_inner_tile (int x, int y, int width, int height, int who)
{
  int r;

  monitoring_start_tile (who);

  r = do_inner_tile_reg (x, y, width, height);

  monitoring_end_tile (x, y, width, height, who);

  return r;
}


////////////
// SEQ
////////////
unsigned life_compute_seq (unsigned nb_iter)
{
  for (unsigned it = 1; it <= nb_iter; it++) {
    int change = 0;

    monitoring_start_tile (0);

    for (int i = 0; i < DIM; i++)
      for (int j = 0; j < DIM; j++)
        change |= compute_new_state (i, j);

    monitoring_end_tile (0, 0, DIM, DIM, 0);

    swap_tables ();

    if (!change)
      return it;
  }

  return 0;
}

////////////
// SEQ vec
////////////
unsigned life_compute_seq_vec (unsigned nb_iter)
{
  for (unsigned it = 1; it <= nb_iter; it++) {
    int change = 0;

    monitoring_start_tile (0);

    for (int i = 0; i < DIM; i++)
      for (int j = 0; j < DIM; j+=32)
        change |= compute_new_state_vec (i, j);

    monitoring_end_tile (0, 0, DIM, DIM, 0);

    swap_tables ();

    if (!change)
      return it;
  }

  return 0;
}

////////////
// OMP VEC
////////////
unsigned life_compute_omp_vec (unsigned nb_iter)
{
  unsigned res = 0;

  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

#pragma omp parallel for schedule(static) collapse(2) reduction(|:change)
    for (int y = 0; y < DIM; y += TILE_H)
      for (int x = 0; x < DIM; x += TILE_W)
          change |= do_tile_vec (x, y, TILE_W, TILE_H, omp_get_thread_num());

    swap_tables ();

    if (!change) { // we stop when all cells are stable
      res = it;
      break;
    }
  }

  return res;
}

////////////
// LAZY VEC
////////////
void life_init_lazy (){
    life_init();
    change_set(1);
}
unsigned life_compute_lazy(unsigned nb_iter) {
    unsigned res = 0;

    for (unsigned it = 1; it <= nb_iter; it++) {
        unsigned change = 0;

#pragma omp parallel for schedule(static) collapse(2) reduction(|:change)
        for (int y = 0; y < DIM; y += TILE_H)
            for (int x = 0; x < DIM; x += TILE_W)
                if (test_change(y / TILE_H, x / TILE_W)) {
                    next_change_table(y / TILE_H, x / TILE_W) =
                        do_tile_vec(x, y, TILE_W, TILE_H, omp_get_thread_num());
                    change |= next_change_table(y / TILE_H, x / TILE_W);
                }

        change_set(0);
        swap_tables();

        if (!change) {  // we stop when all cells are stable
            res = it;
            break;
        }
    }

    return res;
}


///////////////////////////// Initial configs
