#include <omp.h>
#include <stdbool.h>
#include <string.h>
#include <sys/mman.h>
#include <immintrin.h>

#include "init_configs.h"
#include "vectorization.h"


static unsigned cpu_y_part;
static unsigned gpu_y_part;

void change_seto(int t){
    memset(_t_change, t, DIM/TILE_W * DIM/TILE_H * sizeof (cell_t));
}

static long gpu_duration = 0, cpu_duration = 0;

static int much_greater_than (long t1, long t2)
{
  return (t1 > t2) && ((t1 - t2) * 100 / t1 > THRESHOLD);
}

static inline void swap_tables (void)
{
  cell_t *tmp = _table;

  _table           = _alternate_table;
  _alternate_table = tmp;

  tmp = _t_change;
  _t_change        = _a_t_change;
  _a_t_change      = tmp;
}

static int compute_new_state_veco (int y, int x)
{
  __m256i n = _mm256_setzero_si256 ();
  __m256i me = _mm256_loadu_si256 ((__m256i*) &cur_table(y,x));
  unsigned change = 0;

  if (y > 0 && y < DIM - 1) {
  __m256i neighbors_upper_line = _mm256_loadu_si256 ((__m256i*) &cur_table(y-1,x));
  __m256i neighbors_lower_line = _mm256_loadu_si256 ((__m256i*) &cur_table(y+1,x));

  __m256i neighbors_horizontal_sum = _mm256_add_epi8(neighbors_upper_line, me);
  neighbors_horizontal_sum = _mm256_add_epi8(neighbors_horizontal_sum, neighbors_lower_line);

  uint8_t shifting_right[33];
  shifting_right[32] = 0;
  _mm256_storeu_si256((__m256i *)shifting_right, neighbors_horizontal_sum);
  __m256i neighbors_hsum_right_shifted = _mm256_loadu_si256((__m256i *)(shifting_right+1));
  __m256i neighbors_hsum_left_shifted = _mm256_shift_left(neighbors_horizontal_sum);

  if (x > 0){
    uint8_t left_hand_pixel_neighbors[32];
    _mm256_storeu_si256((__m256i *)left_hand_pixel_neighbors, neighbors_hsum_left_shifted);
    left_hand_pixel_neighbors[0] += cur_table(y-1, x - 1)
            + cur_table(y, x - 1)
            + cur_table(y+1, x - 1);
    neighbors_hsum_left_shifted = _mm256_loadu_si256((__m256i *)left_hand_pixel_neighbors);
  }
  if (x < DIM - 32){
    uint8_t right_hand_pixel_neighbors[32];
    _mm256_storeu_si256((__m256i *)right_hand_pixel_neighbors, neighbors_hsum_right_shifted);
    right_hand_pixel_neighbors[31]+= cur_table(y-1, x + 32)
            + cur_table(y, x + 32)
            + cur_table(y+1, x + 32);
    neighbors_hsum_right_shifted = _mm256_loadu_si256((__m256i *)right_hand_pixel_neighbors);
  }

  __m256i nb_neighbors = _mm256_add_epi8(neighbors_horizontal_sum, neighbors_hsum_left_shifted);
  n = _mm256_add_epi8(nb_neighbors, neighbors_hsum_right_shifted);

  __m256i three = _mm256_set1_epi8(3);
  __m256i three_plus_me = _mm256_add_epi8(three, me);

  __m256i nb_of_neighbors = _mm256_cmpeq_epi8(n, three);
  __m256i nb_of_neighbors_plus_me = _mm256_cmpeq_epi8(n, three_plus_me);

  n = _mm256_or_si256(nb_of_neighbors, nb_of_neighbors_plus_me);
  n = _mm256_and_si256(n, _mm256_set1_epi8(1));

  __m256i mask = _mm256_cmpeq_epi8(n, me);
  mask = _mm256_cmpeq_epi8(mask, _mm256_setzero_si256());
  int test = hsum_8x32(mask);
  if (test)
    change |= 1;

  }

  uint8_t zeroing_left_border[32] = {[0 ... 31] = 1};
  uint8_t zeroing_right_border[32] = {[0 ... 31] = 1};
  zeroing_left_border[0] = 0;
  zeroing_right_border[31] = 0;
  if (x == 0)
    n = _mm256_and_si256 (_mm256_loadu_si256((__m256i *)&zeroing_left_border), n);
  if (x == DIM - 32)
    n = _mm256_and_si256 (_mm256_loadu_si256((__m256i *)&zeroing_right_border), n);


  _mm256_store_si256((__m256i *)&next_table(y, x), n);

  return change;
}

static int do_tile_vec_rego (int x, int y, int width, int height)
{
  int change = 0;

  for (int i = y; i < y + height; i++)
    for (int j = x; j < x + width; j+=32)
      change |= compute_new_state_veco (i, j);

  return change;
}

static int do_tile_veco (int x, int y, int width, int height, int who)
{
  int r;

  monitoring_start_tile (who);

  r = do_tile_vec_rego (x, y, width, height);

  monitoring_end_tile (x, y, width, height, who);

  return r;
}

//////////////////////////////////// OPENCL /////////////////////////////////////////////////

unsigned lifeo_invoke_ocl (unsigned nb_iter)
{
  size_t global[2] = {GPU_SIZE_X, gpu_y_part};   // global domain size for our calculation
  size_t local[2]  = {GPU_TILE_W, GPU_TILE_H}; // local domain size for our calculation
  cl_int err;

  cl_event kernel_event;
  long t1, t2;
  int gpu_accumulated_lines = 0;


  monitoring_start_tile (easypap_gpu_lane (TASK_TYPE_COMPUTE));

  for (unsigned it = 1; it <= nb_iter; it++) {

    //   // Load balancing
    if (gpu_duration) {
      if (much_greater_than (gpu_duration, cpu_duration) &&
          gpu_y_part > GPU_TILE_H) {
        gpu_y_part -= GPU_TILE_H;
        cpu_y_part += GPU_TILE_H;
        global[1] = gpu_y_part;
      } else if (much_greater_than (cpu_duration, gpu_duration) &&
                 cpu_y_part > GPU_TILE_H) {
        cpu_y_part -= GPU_TILE_H;
        gpu_y_part += GPU_TILE_H;
        global[1] = gpu_y_part;
      }
    }

    // Set kernel arguments
    err = 0;
    err |= clSetKernelArg (compute_kernel, 0, sizeof (cl_mem), &cur_buffer);
    err |= clSetKernelArg (compute_kernel, 1, sizeof (cl_mem), &next_buffer);
    err |= clSetKernelArg (compute_kernel, 2, sizeof (unsigned), &cpu_y_part);

    check (err, "Failed to set kernel arguments");

    err = clEnqueueNDRangeKernel (queue, compute_kernel, 2, NULL, global, local,
                                  0, NULL, &kernel_event);
    check (err, "Failed to execute kernel");

    // Swap buffers
    {
      cl_mem tmp  = cur_buffer;
      cur_buffer  = next_buffer;
      next_buffer = tmp;
    }

    t1 = what_time_is_it ();

#pragma omp parallel for schedule(static) collapse(2)
    for (int y = 0; y < cpu_y_part; y += TILE_H)
      for (int x = 0; x < DIM; x += TILE_W)
          do_tile_veco (x, y, TILE_W, TILE_H, omp_get_thread_num());
    swap_tables ();

    t2           = what_time_is_it ();
    cpu_duration = t2 - t1;

    clFinish (queue);

    err = clEnqueueWriteBuffer (queue, cur_buffer, CL_TRUE, 0,
                                  DIM * cpu_y_part * sizeof (char), _table, 0,
                                  NULL, NULL);
    check (err, "Failed to write to buffer");

    err = clEnqueueReadBuffer (queue, cur_buffer, CL_TRUE, DIM * cpu_y_part * sizeof (char),
                                  TILE_H * gpu_y_part * sizeof (char), _table + DIM * cpu_y_part * sizeof (char), 0,
                                  NULL, NULL);
    check (err, "Failed to read to buffer");


    gpu_duration = ocl_monitor (kernel_event, 0, cpu_y_part, global[0],
                                    global[1], TASK_TYPE_COMPUTE);
    clReleaseEvent (kernel_event);
    gpu_accumulated_lines += gpu_y_part;
  }

  monitoring_end_tile (0, 0, DIM, DIM, easypap_gpu_lane (TASK_TYPE_COMPUTE));

  PRINT_DEBUG ('u', "In average, GPU took %.1f%% of the lines\n",
            (float)gpu_accumulated_lines * 100 / (DIM * nb_iter));

  return 0;
}

// Only called when --dump or --thumbnails is used

void life_refresh_img_ocl(void) {
  cl_int err;
  err = clEnqueueReadBuffer(queue, cur_buffer, CL_TRUE, 0,
                            sizeof(char) * DIM * DIM, _table, 0, NULL,
                            NULL);
  check(err, "Failed to read buffer from GPU");
  life_refresh_img();
}
