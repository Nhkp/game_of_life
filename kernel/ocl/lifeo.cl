#include "kernel/ocl/common.cl"

/*__kernel void life_ocl(__global unsigned *img) {*/
/*int x = get_global_id(0);*/
/*int y = get_global_id(1);*/
/*}*/

__kernel void lifeo_ocl(__global char *in, __global char *out, unsigned offset) {
    __local unsigned char sum[(GPU_TILE_H)][(GPU_TILE_W)];
    int x = get_global_id(0);
    int y = get_global_id(1) + offset;
    int xloc = get_local_id(0);
    int yloc = get_local_id(1);

    if (x == 0 || y == 0 || x == DIM - 1 || y == DIM - 1)
        out[y * DIM + x] = 0;
    else {
        sum[yloc][xloc] = 0;
        sum[yloc][xloc] += in[(y - 1) * DIM + x - 1];
        sum[yloc][xloc] += in[(y)*DIM + x - 1];
        sum[yloc][xloc] += in[(y + 1) * DIM + x - 1];

        sum[yloc][xloc] += in[(y - 1) * DIM + x];
        sum[yloc][xloc] += in[(y + 1) * DIM + x];

        sum[yloc][xloc] += in[(y - 1) * DIM + x + 1];
        sum[yloc][xloc] += in[(y)*DIM + x + 1];
        sum[yloc][xloc] += in[(y + 1) * DIM + x + 1];
        barrier(CLK_LOCAL_MEM_FENCE);
        out[y * DIM + x] = sum[yloc][xloc] == 3 || (sum[yloc][xloc] == 2 &&
                                                    in[y * DIM + x] > 0);
    }
}

// DO NOT MODIFY: this kernel updates the OpenGL texture buffer
// This is a life-specific version (generic version is defined in common.cl)
__kernel void life_update_texture(__global char *cur,
                                  __write_only image2d_t tex) {
    int y = get_global_id(1);
    int x = get_global_id(0);
    write_imagef(tex, (int2)(x, y),
                 color_scatter(cur[y * DIM + x] * 0xFFFF00FF));
}
