#include "kernel/ocl/common.cl"


__kernel void rotation90_ocl (__global unsigned *in, __global unsigned *out)
{
  int x = get_global_id (0);
  int y = get_global_id (1);

  int xloc = get_local_id (0);
  int yloc = get_local_id (1);

  __local unsigned tile [GPU_TILE_H][GPU_TILE_W];

  tile [xloc][yloc] = in [y * DIM + x];

  barrier (CLK_LOCAL_MEM_FENCE);

  out [yloc*DIM +xloc + DIM * (DIM - x + xloc - GPU_TILE_W) + y - yloc] = tile [GPU_TILE_H - yloc - 1][xloc];

}

